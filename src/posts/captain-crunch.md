---
title: Writing Crunchy Rulesets For Your Tabletop Games
layout: layouts/post.html
coverImage: /assets/images/cover-images/CaptainCrunch.png
blurb: Crunch is what separates a well-built wargame or tabletop roleplaying game from when you played Cowboys and Indians as a kid. So how do you write good crunch?
date: 2023-08-02
---
A couple weeks ago, Justin Vandermeer (who runs [Shout Crow Press](https://shoutingcrow.itch.io/)) reached out for help with his game KILL THE DUKE. He was having trouble writing rules that helped put players into the role of a medieval serf attempting to kill their liege lord with the help of a magical sword - or so he thought. I remember him telling me that he felt that KILL THE DUKE didn't have a good solo system, at which point I fired back with "How is 'ascend the tower and kill the Duke' not a functional solo system?" After some more back and forth, we got to the real meat of things, which is "why do people enjoy crunch?" And in order to answer that, first we have to answer "what is crunch?"
## What Is Crunch?
"Crunch" means different things to different people, but at its core, you can define a crunchy game as one that has complex interactions in the mechanics. In other words, "If I take Action A, then this happens which also causes this happen, which means I can't do this..." and so on and so forth.

In addition to signalling mechanical *complexity*, crunch also usually signifies mechanical *diversity* - that is, that there are more differences to dwarves and elves than needing different-sized barstools.
![](/assets/images/captain-crunch/elf-versus-dwarf.jpeg)
## Why Do People Like Crunch?
So what is it about crunch that people like? Generally, crunch provides for more rewarding interactions than the "I shot you!"/“Uh-Uh, you missed!” games of Cowboys and Indians from when you were a kid. Having an actual ruleset (or at least some third party to adjudicate disputes) keeps every player on the same basic playing field and helps make the game more fun and rewarding for everyone.

Beyond grounding people in the same basics, the mechanical diversity afforded by crunchy rulesets means that the creatures and concepts espoused by the ruleset look different and feel different because they play differently. If a human's base movement speed is 6" and an elf moves 8" to a dwarf's 4", players will intuit that elves are fast and graceful and dwarves slow and purposeful without necessarily needing fluff to tell them so.
## How Do I Write A Crunchy Ruleset?
Before you sit down to write your rules, there's two things to consider:

1. What kind of game you're writing
2. The "fluff" - the narrative you are trying to convey to the player with your game

When it comes to writing a crunchy ruleset, the type of game you're writing matters. A crunchy ruleset for a fantasy tabletop roleplaying game will look very different from a wargame set during the [War of the Spanish Succession](https://en.wikipedia.org/wiki/War_of_the_Spanish_Succession), for example. 

Once you've decided what kind of game you're writing, it's time to consider your "fluff". Fluff is the narrative you are trying to convey to the player with your game. Even the most crunchy, bony, grognard-y rulesets have some fluff, although that fluff might be historical context rather than written fiction. Narrative is important because it gives your players a reason to invest in your ruleset - even if that reason is simply "I have two groups of toy soldiers and I want to play a game with them." A good example of this is *Blackmoor*, a tabletop roleplaying and wargaming setting designed by Dave Arneson for his games of *Chainmail*, a ruleset written by Gary Gygax.
![](/assets/images/a-brief-history-of-miniature-wargaming/chainmail.jpg)
Once you have a narrative in mind, use that to define the basics of the rules you're writing. For example, if you're writing rules that portray the Peloponnesian War, then you need to create stats for hoplites at the very minimum.

After you define those basics, it's time to add your mechanical diversity. Using our Peloponnesian War example from earlier, there's no immediate mechanical diversity - a hoplite is a hoplite is a hoplite. So how do you add it? There's a couple ways:

1. Adding special rules
2. Tweaking profiles
3. Both of the above

Special rules are probably the easiest way - you might give the Spartan hoplites a "Spartan Courage" special rule that says that they don't get pushed back if they lose combat. That makes your Spartan hoplites play differently from from "basic" hoplites. The downside to adding special rules is that once you add one, you can end up with "special rules glut", where, since special rules are the way you've chosen to make characters/units/factions stand out, _every_ unit gets one. Having too many special rules will make the game much less fun and enjoyable than having too few.

If you can't (or don't want to) add special rules, tweaking unit profiles is the next best thing. If a standard hoplite's Morale is 4, a Spartan hoplite might have a Morale of 6 to represent that Spartan courage we talked about earlier. Tweaking unit profiles is a little more messy than adding special rules, though, because every time you tweak the profile, it has to (or at least it should) go through playtesting again to make sure that the tweaks you made line up with your vision for the unit or character in question.

Probably the thing that provides the most "complex interactions" is a combination of both. Each unit or character in your game has its own unique profile _and_ its own unique special rules. Take, for example, Warhammer 40,000's poster-boy unit, the Space Marines. Not only do Space Marines have their own unique unit profiles, they also have their own unique special rule, *And They Shall Know No Fear*, which lets them auto-pass Morale tests. Combined, the unique unit profiles and unique special rules mean that Space Marines - an order of warrior-monks gene-enhanced to be the Imperium's super-soldiers - play very differently from the other factions in the game.
![](/assets/images/captain-crunch/special_rule.png)

How you add crunch to your rulesets is ultimately up to you - what matters, though, is delivering enough complex, meaningful interactions that your players keep coming back to your rulesets again and again.

## Balancing Rule Volume And Complexity
When it comes to writing crunchy rulesets, one of the big things to keep in mind is volume doesn't equal meaningful complexity. Just because your game has lots of rules, doesn't mean that it will be provide the player with meaningfully complex interactions. Rather, tons of rules are more likely to bog down both the player and the game - see SPI's 1978 wargame [The Campaign for North Africa](https://en.wikipedia.org/wiki/The_Campaign_for_North_Africa) as an example of this. So how do you balance rule volume with rule complexity?

The best way to handle this isn't so much a matter of complexity vs volume, but moreso matters of readability, presentation and the overall intuitiveness of rules as presented. Written rules tend to exist because they're answers to questions that players have asked. So, essentially, write only as many rules as your players have questions about the system. Once your system answers most (+/-95%) of a player's system-related questions, you can consider it complete.



