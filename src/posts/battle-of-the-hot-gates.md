---
title: Battle of the Hot Gates
layout: layouts/post.html
coverImage: /assets/images/cover-images/BattleOfTheHotGates.png
blurb: Thermopylae is one of the most famous last stands in history. Now you can fight it out yourself with this scenario for Othismos!
date: 2023-07-19
---

## Introduction
The year is 480 B.C.E. The place, Thermopylae. Across the narrow pass stands the thousands-strong Persian army, ready to bring Greece to her knees. With you stand seven thousand Lacedaemonians, Mantineans, Tegeans, Thespians, Phocians, Locrians and others - Greeks all, ready to stand against the menace invading their shores. News has also reached you that the main Spartan army is on its way: all you have to do is hold off the Persians as long as possible.

## The Forces
The Greeks have 45 ranks of hoplite infantry. Six of those ranks are Spartans, and have the following special rule:

**Spartan Courage**: Spartans are not pushed back when they take casualties.

The Persians have 60 ranks of infantry and archers, including five ranks of Immortals, who have the following special rule:

**Constant Strength**: If the Immortals do not lose a full rank by the end of combat, replace the lost models at the back of the unit.

## Setup
Play the game on a 6' x 4' board. Deploy the Greeks up to half the table's width forward of their board edge, with a wall placed halfway into their deployment zone. Any units immediately behind the wall get +1 Push when being charged by units beyond the wall. 

Deploy the Persians up to 12" forward of their board edge.

The game is played for six turns.

## Victory Conditions
The Greeks score a major victory if there are still Greek units on the field and Leonidas is still alive by the end of the sixth turn.

The Greeks score a minor victory if there are still Greek units on the field by the end of the sixth turn.

The Persians score a victory if there are no Greek units left on the field by the end of the sixth turn.

If you'd like this scenario as a PDF, you can find it [here](https://no-name-games.com/downloads).

