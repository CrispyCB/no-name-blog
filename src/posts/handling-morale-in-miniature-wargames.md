---
title: Handling Morale In Miniature Wargames
layout: layouts/post.html
coverImage: /assets/images/cover-images/ThousandYardStare.png
blurb: Morale rules are one of those things that are conspicuous when they're either absent or done badly. How do you write good morale rules, though?
date: 2023-07-05
---
When it comes to miniature wargames rulesets, morale rules are one of those things that are conspicuous when they're either absent or done badly. What is "morale", though? And how do you write good morale rules?
## What Is Morale?
Morale is usually defined as "the confidence, enthusiasm, and discipline of a person or group at a particular time", or something similar. So how does that translate to tabletop wargames? Typically, "morale" is represented by some statistic (Leadership, Motivation, Resolve, Will or similar) that is then tested against at pre-determined intervals (when a unit takes _X_ number of casualties, if a unit leader is killed, etc). If the unit passes the morale check, it stays put. If not, bad things usually start happening, ranging from the worsened combat effectiveness all the way to outright routing.
## Morale As A Finite Resource
While morale is typically treated as something that functions as a pass-fail (units are OK until they aren't), there is evidence that morale is much more finite than that. A group of researchers from the  Florida State, Florida Atlantic, and San Diego State universities studied the concept of "ego depletion" - that is, that willpower is a finite resource. What they found was this:

_Saying no to every naughty impulse, from raiding the refrigerator to skipping class, requires a little bit of willpower fuel, and once you spend that fuel it becomes harder to say no the next time. ... As your ego depletes, your automatic processes get louder, and each successive attempt to take control of your impulses is less successful than the last._

So how does this apply to morale? The things that discipline guards against - turning tail and running, or hiding in your trench till the shooting stops - are some of the "naughty impulses" that the quote above talks about. They're things that any unit of soldiers has to take a conscious action not to do. However, that conscious action gets easier and easier the more training/discipline a unit of soldiers has to fall back on: a unit of untrained conscripts is much more likely to rout when faced with oncoming fire than a group of hardened veterans.
## Morale As A Determining Factor
![](/assets/images/handling-morale-in-miniature-wargames/French-infantry-detained-before-German-fences.jpg)
When it comes to winning a battle, morale carries much more weight than anything else - it's much easier to make someone run than it is to kill them outright. So how does that translate to writing wargaming rules? Well, it means that, from jump, units will seldom fight "to the last man" - there will be some point long before that where they surrender or flee. It's one of the reasons why I never liked the "Last Man Standing" rules in Warhammer 40,000 - that "last man" is far more likely to turn tail and book it than he is to stay on the battlefield. 

Okay, so we've gotten rid of "Last Man Standing" and its ilk - now what? Decide when and how the units in your game will break and retreat. The tried-and-true "wargaming break test" is 50% - that is, a unit will only start testing for morale once it's received 50% (or more) casualties. History, hower, tells us that that's not the case, that units (and armies above them) break long before they reach 50% casualties - for example, the French at Agincourt retreated after losing 6,000 of their 15,000 men (about 40%). At Crecy, the French only lost 20% (4,000 out of about 20,000) before they retreated. 

You _can_ still use 50% as a break metric - it's a standard for a reason - but it's much less realistic and makes it that much harder to use morale as a gameplay element unless all of the armies in the setting have crappy morale to begin with.

What to do instead? There are a couple of options: either have morale checks happen (with appropriate modifiers) every time a unit takes casualties, or make morale a function of weight of fire. What do I mean by that? Some games, such as One Page Rules's [_Covering Fire_](https://drive.google.com/file/d/1ELuqacVA6ocag93hGT0mEZfSEtD3y_pP/view) and _Crossfire_ before it, determine a unit's ability to function based on the amount of fire it receives from the enemy.
![](/assets/images/handling-morale-in-miniature-wargames/shooting_rules.png)
Both of the options I suggested turn morale from a pass-fail mechanic into something that's much more gradual. Units accumulate stress until finally they break. This, I think, better represents how morale actually functions on a battlefield - a spending of willpower fuel that increases or decreases based on accumulated stressors. 
## A Question Of Leadership
So how do you handle Leadership in a graduated system like this? You have a couple options, depending on the scale of the game you're writing. If you're running something smaller scale (platoon level or below), you can give your sergeants/leaders different morale values and have squads use those for their tests as long as the leader is still alive. At company level (or larger), have command units give infantry/vehicle units a +1/+2 to their morale checks (depending on the type of command unit, of course).
## Final Thoughts
Hopefully this shines a bit of a spotlight on morale mechanics and how they can be more than just pass-fail single-dice-roll tests. I'm not advocating a "best method" for morale tests - just that, if you're writing morale rules, there's other ways to do it than what's been the "bog standard" of gaming for years.
